/*
 * Copyright 2013-2017 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core.services.projects;

import static com.google.common.base.Preconditions.checkNotNull;

import de.smartics.projectdoc.atlassian.confluence.tools.projects.ProjectCreationService;
import de.smartics.projectdoc.atlassian.confluence.tools.projects.ProjectCreationServiceAccessor;

import com.atlassian.spring.container.ContainerManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

import javax.annotation.CheckForNull;

/**
 * Wrapper to discover an implementation of the project creation service.
 */
public class CoreProjectCreationServiceAccessor
    implements ProjectCreationServiceAccessor {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * Reference to the logger for this class.
   */
  private static final Logger LOG =
      LoggerFactory.getLogger(CoreProjectCreationServiceAccessor.class);

  /**
   * The name of the service class implementation.
   */
  private static final String SERVICE_CLASS =
      "de.smartics.projectdoc.extension.maven.projects.task.PomCreationService";

  // --- members --------------------------------------------------------------

  private final ApplicationContext applicationContext;

  /**
   * Optional service to be set manually.
   */
  private ProjectCreationService service;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public CoreProjectCreationServiceAccessor(
      final ApplicationContext applicationContext) {
    this.applicationContext =
        checkNotNull(applicationContext, "applicationContext");
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void notifyRemovalOf(final ClassLoader classloader) {
    if (service != null) {
      final ClassLoader loader = service.getClass().getClassLoader();
      if (loader == classloader) {
        if (LOG.isDebugEnabled()) {
          LOG.debug("Unloading service since classloader matches.");
        }
        service = null;
      } else {
        try {
          classloader.loadClass(SERVICE_CLASS);
          if (LOG.isDebugEnabled()) {
            LOG.debug(
                "Unloading service since classloader loads service class.");
          }
          service = null;
        } catch (final Exception e) {
          // Ok, classloader not responsible for loading service implementation
        }
      }
    }
  }

  public void notifyRemovalOf(final String key) {
    if ("de.smartics.atlassian.smartics-projectdoc-maven-extension"
        .equals(key)) {
      service = null;
    }
  }

  @Override
  @CheckForNull
  public ProjectCreationService getService() {
    ProjectCreationService local = service;
    if (local == null) {
      local = createService();
      service = local;
    }

    return local;
  }

  @CheckForNull
  private ProjectCreationService createService() {
    try {
      final Class<?> serviceFactoryClass = getServiceFactoryClass();
      if (serviceFactoryClass != null) {
        return ((ServiceFactory) applicationContext
            .getAutowireCapableBeanFactory().createBean(serviceFactoryClass,
                AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, false))
                    .getService();
      } else {
        return ContainerManager.getComponent("projectdocProjectCreationService",
            ProjectCreationService.class);
      }
    } catch (final Exception e) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Could create service '{}': {}",
            ProjectCreationService.class.getName(), e.getMessage());
      }
    }
    return null;
  }

  @CheckForNull
  private Class<?> getServiceFactoryClass() {
    try {
      getClass().getClassLoader().loadClass(SERVICE_CLASS);
      return ServiceFactory.class;
    } catch (final Exception e) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Cannot locate service: {}", SERVICE_CLASS);
      }
      return null;
    }
  }

  // --- object basics --------------------------------------------------------

}

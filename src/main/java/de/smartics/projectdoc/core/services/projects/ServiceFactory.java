/*
 * Copyright 2013-2017 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core.services.projects;

import de.smartics.projectdoc.atlassian.confluence.tools.projects.ProjectCreationService;
import de.smartics.projectdoc.atlassian.confluence.tools.service.OptionalServiceFactory;

import org.osgi.framework.BundleContext;

/**
 * Maps the {@link ProjectCreationService} to a bundle context.
 */
class ServiceFactory extends OptionalServiceFactory<ProjectCreationService> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  ServiceFactory(final BundleContext bundleContext) {
    super(bundleContext, ProjectCreationService.class);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}

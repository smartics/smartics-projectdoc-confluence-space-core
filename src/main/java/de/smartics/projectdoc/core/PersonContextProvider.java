/*
 * Copyright 2013-2017 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core;

import de.smartics.projectdoc.atlassian.confluence.api.doctypes.Doctype;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;

/**
 * Provides a default name and short description for versions.
 */
public class PersonContextProvider extends ProjectDocContextProviderExt {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The given name of the person as specified in the wizard to create a
   * document of type 'Person'.
   * <p>
   * The value of this constant is {@value}.
   * </p>
   */
  private static final String KEY_GIVEN_NAME =
      "projectdoc.doctype.person.name.givenName";

  /**
   * The family name of the person as specified in the wizard to create a
   * document of type 'Person'.
   * <p>
   * The value of this constant is {@value}.
   * </p>
   */
  private static final String KEY_FAMILY_NAME =
      "projectdoc.doctype.person.name.familyName";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public PersonContextProvider() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  /**
   * Provides properties to the blueprint context.
   * <p>
   * Constructs the document name from the given and family name of the person.
   * </p>
   *
   * @param handler with access to the context.
   */
  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final String givenName = (String) blueprintContext.get(KEY_GIVEN_NAME);
    final String familyName = (String) blueprintContext.get(KEY_FAMILY_NAME);
    final String documentName = givenName + ' ' + familyName;
    blueprintContext.put(Doctype.NAME, documentName);

    final BlueprintContext parentContext =
        super.updateBlueprintContext(blueprintContext);

    return parentContext;
  }

  // --- object basics --------------------------------------------------------

}

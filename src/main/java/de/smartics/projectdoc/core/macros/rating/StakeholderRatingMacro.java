/*
 * Copyright 2013-2017 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.core.macros.rating;

import de.smartics.projectdoc.atlassian.confluence.document.DocumentProperty;
import de.smartics.projectdoc.atlassian.confluence.document.FragmentRenderer;
import de.smartics.projectdoc.atlassian.confluence.document.ProjectdocDocument;
import de.smartics.projectdoc.atlassian.confluence.document.PropertiesDocument;
import de.smartics.projectdoc.atlassian.confluence.document.PropertiesLoadStrategy;
import de.smartics.projectdoc.atlassian.confluence.persistence.ProjectdocDocumentService;
import de.smartics.projectdoc.atlassian.confluence.query.PropertiesDocumentCreator;
import de.smartics.projectdoc.atlassian.confluence.util.MacroUtils;
import de.smartics.projectdoc.atlassian.confluence.util.ProjectdocUser;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import org.apache.commons.lang3.StringUtils;

import java.util.Locale;
import java.util.Map;

/**
 * Renders the stakeholder rating dependent on interest and power.
 */
public class StakeholderRatingMacro extends BaseMacro implements Macro {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The name to the parameter that references the document. If the document is
   * not set, the current page is used.
   */
  private static final String PARAM_DOCUMENT = "document";

  // --- members --------------------------------------------------------------

  /**
   * Helper to access locale dependent information.
   */
  private final ProjectdocUser user;

  /**
   * Helper to create the instance of a properties document.
   */
  private final PropertiesDocumentCreator creator;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  protected StakeholderRatingMacro(final ProjectdocUser user,
      final XhtmlContent xhtmlContent, final SpaceManager spaceManager,
      final PageManager pageManager, final AttachmentManager attachmentManager,
      final ProjectdocDocumentService storeService) {
    this.user = user;
    this.creator = new PropertiesDocumentCreator(spaceManager, user,
        pageManager, attachmentManager, xhtmlContent, storeService);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public RenderMode getBodyRenderMode() {
    return RenderMode.NO_RENDER;
  }

  @Override
  public BodyType getBodyType() {
    return BodyType.NONE;
  }

  @Override
  public OutputType getOutputType() {
    return OutputType.INLINE;
  }

  @Override
  public boolean hasBody() {
    return false;
  }

  // --- business -------------------------------------------------------------

  protected static String getParameter(final Map<String, String> parameters,
      final String paramName, final String defaultValue) {
    return MacroUtils.getParameter(parameters, paramName, defaultValue);
  }

  @Override
  @SuppressWarnings({"rawtypes", "unchecked"})
  public String execute(final Map parameters, final String body,
      final RenderContext renderContext) throws MacroException {
    try {
      return execute(parameters, body,
          new DefaultConversionContext(renderContext));
    } catch (final MacroExecutionException e) {
      throw new MacroException(e);
    }
  }

  @Override
  public String execute(final Map<String, String> parameters, final String body,
      final ConversionContext conversionContext)
      throws MacroExecutionException {
    final String wikiPage = getParameter(parameters, PARAM_DOCUMENT, null);
    final PropertiesLoadStrategy strategy =
        PropertiesLoadStrategy.createWithBasicArtificialProperties();
    final PropertiesDocument propertiesDocument =
        creator.fetchDocument(conversionContext, wikiPage, strategy);
    final Locale locale = user.fetchLocale();
    final I18NBean i18n = user.createI18n(locale);
    if (propertiesDocument == null) {
      final String errorMessage = RenderUtils.blockError(i18n.getText(
          "de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core.projectdoc-stakeholder-rating-macro.no-document"),
          "");
      return errorMessage;
    }
    final ProjectdocDocument document =
        propertiesDocument.getProjectdocDocument();

    final String stakeholderRatingValue =
        handleRating(creator, conversionContext, i18n, document);

    return stakeholderRatingValue;
  }

  protected static String handleRating(final FragmentRenderer creator,
      final ConversionContext conversionContext, final I18NBean i18n,
      final ProjectdocDocument document) {
    final String powerRatingName =
        i18n.getText("projectdoc.doctype.stakeholder.analysis.powerRating");
    final Integer powerRating =
        calcRating(creator, conversionContext, document, powerRatingName);

    final String interestRatingName =
        i18n.getText("projectdoc.doctype.stakeholder.analysis.interestRating");
    final Integer interestRating =
        calcRating(creator, conversionContext, document, interestRatingName);

    final String stakeholderRating = i18n
        .getText("projectdoc.doctype.stakeholder.analysis.stakeholderRating");
    final String stakeholderRatingValue =
        calculateRating(i18n, powerRating, interestRating);
    document.put(stakeholderRating, stakeholderRatingValue);
    return stakeholderRatingValue;
  }

  private static Integer calcRating(final FragmentRenderer renderer,
      final ConversionContext conversionContext,
      final ProjectdocDocument document, final String ratingName) {
    final DocumentProperty property = document.getProperty(ratingName);
    if (property != null) {
      property.renderAndSet(conversionContext, renderer);
      final String renderedValue = property.getNormalizedRenderedValue();
      if (StringUtils.isNotBlank(renderedValue)) {
        return renderedValue.length();
      }
    }

    return null;
  }

  private static String calculateRating(final I18NBean i18n,
      final Integer powerRating, final Integer interestRating) {
    final String rating;
    if (powerRating != null && interestRating != null) {
      if (powerRating > 2) {
        if (interestRating > 2) {
          rating = i18n.getText(
              "projectdoc.doctype.stakeholder.analysis.stakeholderRating.engageFully");
        } else {
          rating = i18n.getText(
              "projectdoc.doctype.stakeholder.analysis.stakeholderRating.keepSatisfied");
        }
      } else {
        if (interestRating > 2) {
          rating = i18n.getText(
              "projectdoc.doctype.stakeholder.analysis.stakeholderRating.keepInformed");
        } else {
          rating = i18n.getText(
              "projectdoc.doctype.stakeholder.analysis.stakeholderRating.monitor");
        }
      }
    } else {
      rating = StringUtils.EMPTY;
    }

    return rating;
  }

  // --- object basics --------------------------------------------------------

}

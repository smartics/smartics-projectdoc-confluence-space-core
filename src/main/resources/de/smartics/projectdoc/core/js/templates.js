/*
 * Copyright 2013-2017 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Confluence.Blueprint.setWizard(
    'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-charter',
    PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
    'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-type-template',
    PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-docsection',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:projectdoc-blueprint-doctype-excerpt',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-faq',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-generic',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-metadata',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-glossary-item',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-relation',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-organization',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-tour',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-resource',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-role',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-stakeholder',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-topic',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-logstep',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-docmodule',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-subject',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-tag',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-category',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:projectdoc-create-subspace-core-main-template',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-module-type',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-experience-level',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-resource-type',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-topic-type',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-association',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-association-type',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-profile',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-profile-type',
      PROJECTDOC.standardWizard);

Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-profile-type',
      PROJECTDOC.standardWizard);

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-person',
  function(wizard) {
    wizard.on('pre-render.page1Id', function(e, state) {
      state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
    });

      wizard.on('submit.page1Id', function(e, state) {
        var givenName = state.pageData["projectdoc.doctype.person.name.givenName"];
        if (!givenName) {
            alert('Please provide a given name for this person document.');
            return false;
        }

        var familyName = state.pageData["projectdoc.doctype.person.name.familyName"];
        if (!familyName) {
            alert('Please provide a family name for this person document.');
            return false;
        }

        var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
        if (!shortDescription){
            alert('Please provide a short description for this person.');
            return false;
        }

        state.pageData["projectdoc.doctype.common.name"] = givenName + " " + familyName;

        PROJECTDOC.adjustToLocation(state);
    });
      wizard.on('post-render.page1Id', function(e, state) {
          var title = state.wizardData.title;
          if(title) {
            $('#projectdoc\\.doctype\\.common\\.name').val(title);
            $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
          }
        });

      wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-version',
  function(wizard) {
      wizard.on('submit.page1Id', function(e, state) {
        var pom = state.pageData["projectdoc.doctype.pom.reference"];
        var name = state.pageData["projectdoc.doctype.common.name"];
        if(!pom)
        {
            if (!name) {
                alert('Please provide a POM or an ID for this version.');
                return false;
            }

            var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
            if (!shortDescription) {
                alert('Please provide a short description for the document.');
                return false;
            }
        }

		if (!name) {
			var version = "N/A";
			if (pom.endsWith(".pom")) {
				var index = pom.lastIndexOf("-");
				if (index < pom.length - 5) {
					version = 'V' + pom.substring(index + 1, pom.length - 4);
				}
			} else {
				var index = pom.lastIndexOf(":");
				if (index < pom.length - 1) {
					version = 'V' + pom.substring(index + 1);
				}
			}
			state.pageData["projectdoc.doctype.common.name"] = version;
		}

        PROJECTDOC.adjustToLocation(state);
      });

    wizard.on('pre-render.page1Id', function(e, state) {
      state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);

      var url = AJS.contextPath() + "/plugins/servlet/projectdoc/core/services-index?type=ProjectCreationService";
      state.soyRenderContext['pomReferenceFieldStatus'] = 'disabled';
      state.soyRenderContext['pomReferenceFieldTitle'] =  AJS.I18n.getText("projectdoc.doctype.version.blueprint.wizard.pomReferenceField.title.disabled");
      AJS.$.ajax({
            url: url,
            success: function(data, status) {
          if(status === 'success') {
          state.soyRenderContext['pomReferenceFieldStatus'] = ' ';
            state.soyRenderContext['pomReferenceFieldTitle'] =  AJS.I18n.getText("projectdoc.doctype.version.blueprint.wizard.pomReferenceField.title.enabled");
          }
        },
            async: false
        });
      });

    wizard.on('post-render.page1Id', function(e, state) {
        var title = state.wizardData.title;
        if(title) {
          $('#projectdoc\\.doctype\\.common\\.name').val(title);
          $('#projectdoc\\.doctype\\.common\\.shortDescription').focus();
        }
    wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
      });
  });

Confluence.Blueprint
.setWizard(
  'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core:create-doctype-template-quote',
  function(wizard) {
    wizard.on('pre-render.page1Id', function(e, state) {
      state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
    });

    wizard.on('submit.page1Id', function(e, state) {
      var name = state.pageData["projectdoc.doctype.common.name"];
      if (!name) {
        alert('Please provide context for the quote.');
        return false;
      }

      var shortDescription = state.pageData["projectdoc.doctype.quote.text"];
      if (!shortDescription){
        alert('Please provide the text of the quote.');
        return false;
      }

      PROJECTDOC.adjustToLocation(state);
    });

    wizard.on('post-render.page1Id', function(e, state) {
      var title = state.wizardData.title;
      if(title) {
        $('#projectdoc\\.doctype\\.common\\.name').val(title);
        $('#projectdoc\\.doctype\\.quote\\.text').focus();
      }
        wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
        });
  });

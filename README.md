projectdoc Core Doctypes Add-on
===============================

**DEPRECATION WARNING !!!**
Please note that this project is deprecated. Support of the core doctypes is continued by the project 
[smartics-doctype-model-core](https://bitbucket.org/smartics/smartics-doctype-model-core) 
that generates the add-on from a model description.

##Overview

This is a free add-on for the [projectdoc Toolbox](https://www.smartics.eu/confluence/display/PDAC1/) for Confluence.

The add-on provides the core blueprints to create pages for

  * Tags, Subjects, Categories
  * Persons, Stakeholder, Organizations, Roles
  * Topics, Tours, Glossaries, FAQs, Quotes, Resources
  * and some more!

It also provides space blueprints to get started with your documentation project quickly.

##Fork me!
Feel free to fork this project to adjust the templates according to your project requirements.

The projectdoc Core Doctypes Add-on is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

##Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/display/PDAC1/Core+Doctypes)
  * the [add-on on the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-core)